package org.minbox.chapter.nginx.upgrade.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试控制器
 *
 * @author 恒宇少年
 */
@RestController
@RequestMapping(value = "/test")
public class TestController {
    @Autowired
    private ServerProperties serverProperties;

    @GetMapping
    public String hello() {
        return "请求分发到了，端口号：" + serverProperties.getPort() + "的服务，接口访问成功.";
    }
}
